<?php
/**
 * PHP version > 5.5
 *
 * @category Command
 * @package  Acme\Command
 * @author   Rafael Ernesto Espinosa Santiesteban <rernesto.espinosa@gmail.com>
 * @license  MIT <http://www.opensource.org/licenses/mit-license.php>
 * @link     http://fluency.llc.com
 */

namespace Acme\Command;


use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends Command
{
    /**
     * Configures command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('acme:hello')
            ->setDescription('Say hello.');
    }
    /**
     * Executes command
     *
     * @param InputInterface  $input  Command input
     * @param OutputInterface $output Console output
     *
     * @return void
     */
    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $output->writeln('Hello my friend...');
    }
}