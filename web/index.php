<?php

ini_set('display_errors', 0);

require_once __DIR__.'/../vendor/autoload.php';

if (!defined('_ENV_')) {
    define('_ENV_', 'prod');
}

$app = require __DIR__.'/../app/bootstrap.php';

$app->run();
