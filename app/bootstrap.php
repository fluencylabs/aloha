<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Application();

// Set constants
if (!defined('BASEPATH')) {
    define('BASEPATH', realpath(__DIR__ . '/../'));
}
if (!defined('LOGPATH')) {
    define('LOGPATH', BASEPATH . '/var/logs');
}
if (!defined('CACHEPATH')) {
    define('CACHEPATH', BASEPATH . '/var/cache');
}

// Load configurations
$app->register(
    new \Fluency\Silex\Provider\YamlConfigServiceProvider(
        array(
            '%base_path%' => BASEPATH,
            '%log_path%' => LOGPATH,
            '%cache_path%' => CACHEPATH
        )
    ),
    array(
        'config.dir' => BASEPATH . '/app/Resources/config',
        'config.files' => array(
            'config_'._ENV_.'.yml', 'routing.yml'
        ),
    )
);

//Set Timezone
if (isset($app['config']['parameters']['timezone'])) {
    date_default_timezone_set($app['config']['parameters']['timezone']);
}

// Register service providers
foreach ($app['config']['service_providers'] as $serviceProviderConfig) {
    $app->register(
        new $serviceProviderConfig['class'](
            (!isset($serviceProviderConfig['construct_parameters'])) ?
                null:$serviceProviderConfig['construct_parameters']
        ),
        (isset($serviceProviderConfig['parameters']) &&
            null !== $serviceProviderConfig['parameters']) ?
            $serviceProviderConfig['parameters'] : array()
    );
}

// Extends some service providers capabilities
if (isset($app['twig'])) {
    $app['twig'] = $app->extend('twig', function ($twig, $app) {
        $twig->addGlobal('now', time());
        return $twig;
    });
}

// Set controllers using routing configuration
if (isset($app['config']['routing'])) {
    foreach ($app['config']['routing']
             as $controllerServiceName => $controllerConfig) {

        $controllerClass = $controllerConfig['class'];
        $controllerRoutes = $controllerConfig['routes'];

        $app[$controllerServiceName] =
            function () use ($app, $controllerServiceName, $controllerClass) {
                return new $controllerClass;
            };

        foreach ($controllerRoutes as $route) {
            $method = (isset($route['method']))?$route['method']:'get';
            $controller = $app->$method($route['pattern'],
                $controllerServiceName . ':' . $route['action']);
            //Bind route name, then url generator can be used
            if (isset($route['name'])) {
                $controller->bind($route['name']);
            }
        }
    }

    // Handle errors, prod environment
    $app->error(function (\Exception $e, Request $request, $code) use ($app) {
        if (_ENV_ == 'dev') {
            return false;
        }

        // 404.html, or 40x.html, or 4xx.html, or error.html
        $templates = array(
            'errors/'.$code.'.html.twig',
            'errors/'.substr($code, 0, 2).'x.html.twig',
            'errors/'.substr($code, 0, 1).'xx.html.twig',
            'errors/default.html.twig',
        );

        return new Response(
            $app['twig']->resolveTemplate($templates)
                ->render(array('code' => $code)), $code
        );
    });
}

return $app;