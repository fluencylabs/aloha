<?php

use Silex\Application;
use Knp\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputOption;

// Set constants
if (!defined('BASEPATH')) {
    define('BASEPATH', realpath(__DIR__ . '/../'));
}
if (!defined('LOGPATH')) {
    define('LOGPATH', BASEPATH . '/var/logs');
}
if (!defined('CACHEPATH')) {
    define('CACHEPATH', BASEPATH . '/var/cache');
}

$app = new Application();

// Load configurations
$app->register(
    new \Fluency\Silex\Provider\YamlConfigServiceProvider(
        array(
            '%base_path%' => BASEPATH,
            '%log_path%' => LOGPATH,
            '%cache_path%' => CACHEPATH
        )
    ),
    array(
        'config.dir' => BASEPATH . '/app/Resources/config',
        'config.files' => array('console.yml', 'routing.yml', 'services.yml'),
    )
);

//Set Timezone
if (isset($app['config']['parameters']['timezone'])) {
    date_default_timezone_set($app['config']['parameters']['timezone']);
}

// Register service providers
foreach ($app['config']['service_providers'] as $serviceProviderConfig) {
    $app->register(
        new $serviceProviderConfig['class'](
            (!isset($serviceProviderConfig['construct_parameters'])) ?
                null:$serviceProviderConfig['construct_parameters']
        ),
        (isset($serviceProviderConfig['parameters']) &&
            null !== $serviceProviderConfig['parameters']) ?
            $serviceProviderConfig['parameters'] : array()
    );
}

// Extends some service providers capabilities
if (isset($app['twig'])) {
    $app['twig'] = $app->extend('twig', function ($twig, $app) {
        $twig->addGlobal('now', time());
        return $twig;
    });
}

$console = new ConsoleApplication(
    $app, BASEPATH,
    $app['config']['console']['name'], $app['config']['console']['version']
);
$console->getDefinition()
    ->addOption(
        new InputOption(
            '--env', '-e', InputOption::VALUE_REQUIRED,
            'The Environment name.', 'dev'
        )
    );

$console->setDispatcher($app['dispatcher']);

foreach ($app['config']['commands'] as $commandConfig) {
    $commandInstance = new $commandConfig['class'];
    $console->add($commandInstance);
}

return $console;
